<?php

namespace app\modules\controllers;

use Yii;
use app\modules\models\category\Category;
use app\modules\models\category\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\models\image\Image;
/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {

        
        $searchModel = new CategorySearch();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model->load(Yii::$app->request->post())){
            $selectCat= \app\modules\models\category\Category::find()->andwhere(['title'=>$_POST['Category']['title']])->one();
            if($id==$selectCat->id){
                throw new NotFoundHttpException('Select another category .');
            }
            $deleteImg = Image::find()->andwhere(['category'=>$model->slug])->all();
             if($deleteImg){
                        foreach($deleteImg as $img){
                                $img->category = $selectCat->slug;
                                 $img->save(false);
                                }
        if($model->delete()){
            return $this->redirect(['index']);
                }else{throw new NotFoundHttpException('Something wrong.');}
        }else{$model->delete();throw new NotFoundHttpException('0 images in this category.');}
        }
        return $this->render('view', [
            'model' => $model,
        ]);

    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();


        if ($model->load(Yii::$app->request->post()) ) {
            
        if($model->save()){
        return $this->redirect(['view', 'id' => $model->id]);
        }
    }
        return $this->render('create', [
            'model' => $model,
        ]);
    
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $deleteImg = Image::find()->andwhere(['category'=>$model->slug])->all();
            foreach($deleteImg as $img){
                $delImg = Yii::getAlias('@web').'images/photogallery/'.$img->id.'.'.$img->extension;
          if(file_exists($delImg)) {    
        unlink($delImg);
    }}
       if( $model->delete()){ Yii::$app->session->setFlash('success', 'Category was deleted');
        return $this->redirect(['index']);
    };
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

   throw new NotFoundHttpException('The requested page does not exist.');     
    }
}
