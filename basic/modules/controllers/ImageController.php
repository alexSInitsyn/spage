<?php

namespace app\modules\controllers;

use Yii;
use app\modules\models\category\Category;
use app\modules\models\image\Image;
use app\modules\models\image\ImageSearch;
use app\modules\models\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ImageController implements the CRUD actions for Image model.
 */
class ImageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Image models.
     * @return mixed
     */
    public function actionIndex($slug)
    {
        $searchModel = Image::find()->andwhere(['category'=>$slug])->all();
$categoryModel = Category::find()->andwhere(['slug'=>$slug])->one();
$dataProvider = new ActiveDataProvider([
    'query' => Image::find()->andwhere(['category'=>$slug]),
    'pagination'=>['pageSize'=>10,
    'pageSizeParam' => false],
]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'categoryModel'=>$categoryModel,
            'dataProvider'=>$dataProvider
        ]);
    }

    /**
     * Displays a single Image model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Image model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Image();
        $uModel = new UploadForm();

        if ($model->load(Yii::$app->request->post())) 
        { 

            $uModel->imageFile = UploadedFile::getInstance($uModel, 'imageFile');
            $model->extension = $uModel->imageFile->extension;
            
            if($model->extension === 'jpg'|| $model->extension === 'jpeg') {$uploadImg = imagecreatefromjpeg($uModel->imageFile->tempName);}
             else if($model->extension === 'png'){$uploadImg = imagecreatefrompng($uModel->imageFile->tempName);}
            else{ $uploadImg = imagecreatefromgif($uModel->imageFile->tempName);}

                if($model->save()){
                   
                    switch($model->image) {
                        case 1:$watermark=false;break;
                        case 2:$watermark=[1,1];break;
                        case 3:$watermark=[200,1];break;
                        case 4:$watermark=[1,280];break;
                        case 5:$watermark=[200,280];break;
                        }
                       
                        if($watermark){
                            
                           // $img = \yii\imagine\Image::thumbnail( $uModel->imageFile->tempName, 300, 300);
                   
                     \yii\imagine\Image::text(
                        $uModel->imageFile->tempName,
                        Yii::$app->name,
                        '@yii/captcha/SpicyRice.ttf',$watermark, ["fff", 50, 0]
                    )->save( Yii::getAlias('@web').'images/photogallery/'. $model->id . '.' . $uModel->imageFile->extension);
                    }else{
                        $uModel->imageFile->saveAs( Yii::getAlias('@web').'images/photogallery/'. $model->id . '.' . $uModel->imageFile->extension);
                    }
                    Yii::$app->session->setFlash('success', 'upload success');
                    return $this->redirect(['view', 'id' => $model->id]);
                                 }else{
                                        Yii::$app->session->setFlash('error', 'upload error');
                                        return $this->render('create', ['model' => $model, 'uModel'=>$uModel]);
                                        }
         }

   
        return $this->render('create', ['model' => $model, 'uModel'=>$uModel]);
    }

    /**
     * Updates an existing Image model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       

        if ($model->load(Yii::$app->request->post())) {

             if($model->save()){
            return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Image model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $this->findModel($id)->delete();

        unlink(Yii::getAlias('@web').'images/photogallery/'.$id.'.'.$model->extension);
        
        return $this->redirect(['/photo/admin/images/'.$model->category]);


       


    }

    /**
     * Finds the Image model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
