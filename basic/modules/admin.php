<?php

namespace app\modules;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * image module definition class
 */
class admin extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\controllers';

    public function beforeAction($action){

        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->username=='admin'){
            return true;
        } else {
            throw new NotFoundHttpException('Check access right');
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
