<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\page\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
   .gallery-item {
    position: relative;
    text-align: center;
    color: white;
}
.centered {
    opacity: 0.6;
    background-color: black;
    filter: alpha(Opacity=90); /* Прозрачность в IE */
    color: #fff;
    position: absolute;
    top: 90%;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>

<?php 
   echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' =>'_categories',
        'summary' => '',
        'layout' => '{pager}{items}',
        'pager' => [
             'class' => \kop\y2sp\ScrollPager::className(),
             'linkPagerOptions'     => [
                'class' => 'pagination',
           ],
           'linkPagerWrapperTemplate' => '<div class="button-news-more"><div class="wrapper"><div class="paging">{pager}</div></div></div>',
             'triggerOffset'=>100,
             'eventOnReady' => 'function() {{{ias}}.restorePagination();}',
             'eventOnScroll' => 'function() {}', 
        ],
]);

?> 
 
  
 

    
   