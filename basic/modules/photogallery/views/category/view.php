
<?php

use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\page\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' =>'_items',
        'summary' => '',
        'layout' => '{pager}{items}',
        'pager' => [
             'class' => \kop\y2sp\ScrollPager::className(),
             'linkPagerOptions'     => [
                'class' => 'pagination',
           ],
           'linkPagerWrapperTemplate' => '<div class="button-news-more"><div class="wrapper"><div class="paging">{pager}</div></div></div>',
             'triggerOffset'=>100,
             'eventOnReady' => 'function() {{{ias}}.restorePagination();}',
             'eventOnScroll' => 'function() {}', 
        ],
]);
?>


<?php $this->registerJs("window.onload =  function() {
  baguetteBox.run('.item');
};") ?>



 

