<?php

/* @var $this yii\web\View */
/*
$this->title = 'S-page';
foreach($page as $one)
?>
<?php foreach($page as $one):?>
        <h3><?= $one->title?></h3>
        <p class="lead"><?= $one->short_content?></p>
        <?= \yii\bootstrap\Html::a('далее',['page/spage', 'slug'=>$one->slug], ['class'=>'btn btn-success'] )?>
        <p><?= $one->date_update?></p>
    

   

<?php endforeach; ?>

*/

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\models\image\ImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
    
            'c_id',
            'name:ntext',
            'description:ntext',
            array(
                'format' => 'image',
                'value'=>function($data) { return $data->logoUrl; },
                
                   ),
    
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);  ?>


</div>