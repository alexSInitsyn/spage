<?php

namespace app\modules\photogallery\controllers;

use Yii;
use app\modules\models\image\ImageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\modules\models\image\Image;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        
            if (Yii::$app->user->isGuest){
            $searchModel = \app\modules\models\category\Category::find()->andwhere(['status'=>[1,4],])->all();
            $dataProvider = new ActiveDataProvider([
                'query' => \app\modules\models\category\Category::find()->andwhere(['status'=>[1,4],]),
                'pagination'=>['pageSize'=>4,
                'pageSizeParam' => false],
            ]);
            }else if(!Yii::$app->user->isGuest && Yii::$app->user->identity->username=='admin'){
                $searchModel = \app\modules\models\category\Category::find()->andwhere(['status'=>[1,2,3,4],])->all();
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\modules\models\category\Category::find()->andwhere(['status'=>[1,2,3,4],]),
                    'pagination'=>['pageSize'=>4,
                    'pageSizeParam' => false],
                ]);
                
            }else{
                $searchModel = \app\modules\models\category\Category::find()->andwhere(['status'=>[1,2,4],])->all();
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\modules\models\category\Category::find()->andwhere(['status'=>[1,2,4],]),
                    'pagination'=>['pageSize'=>4,
                    'pageSizeParam' => false],
                ]);
            }


            if($searchModel){
           
            
            return $this->render('index', [
                
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            Yii::$app->session->setFlash('error', 'error find category');
            return $this->goHome();
        }

       
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        
      $statusPage = \app\modules\models\category\Category::find()->andwhere(['slug'=>$slug])->one();
        $page = Image::find()->andwhere(['category'=>$slug])->all();
        $dataProvider = new ActiveDataProvider([
            'query' => Image::find()->andwhere(['category'=>$slug]),
            'pagination'=>['pageSize'=>4,
            'pageSizeParam' => false],
        ]);

        if($statusPage && $page){
    
        if($statusPage->status==1 || $statusPage->status==4){
    
            
        return $this->render('view', [
            'page' => $page,
            'dataProvider' => $dataProvider,
           
        ]);
        
        }else if($statusPage->status==2){
            if(!Yii::$app->user->isGuest){
               
                return $this->render('view', [
                    'page' => $page,
                    'dataProvider' => $dataProvider,
                  
                ]); 
            }else{
                throw new NotFoundHttpException('Check access right');
            }
    
            
        }else if($statusPage->status==3){
    
            if(!Yii::$app->user->isGuest && Yii::$app->user->identity->username=='admin'){
                
            
                return $this->render('view', [
                    'page' => $page,
                    'dataProvider' => $dataProvider,
                 
                ]); 
            }else{
            throw new NotFoundHttpException('Check access right');
            }
        }
    
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    
    
    }
}
