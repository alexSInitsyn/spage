<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\models\category\Category;
use app\modules\models\category\CategorySearch;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\models\category\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['id'=>'w0']); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'id'=>'category-title']) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'id'=>'category-slug']) ?>
   

    <?= $form->field($model, 'status')->dropDownList([1=>'guest', 2=>'user', 3=>'admin', 4=>'link'],
['promt'=>'Статус'], ['id'=>'category-status']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
