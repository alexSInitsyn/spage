<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\modules\models\image\Image;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\models\category\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?= Html::a('Create Image', ['/admin/image/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'title',
            'value' => function($model){
                return Html::a($model->title, Url::to(['/photo/admin/images/'.$model->title]));
              },
            'format'=>'raw'],
            'slug',
            'status',
            ['attribute'=>'count',
            'value' => function($model){
                $countPhoto = Image::find()->andwhere(['category'=>$model->slug])->select('title');
                $totalPhoto = $countPhoto->count();
            return $totalPhoto; },
            'format'=>'raw'],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
