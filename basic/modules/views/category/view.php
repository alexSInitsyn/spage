<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap4\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\models\category\Category;
use app\modules\models\category\CategorySearch;


/* @var $this yii\web\View */
/* @var $model app\modules\models\category\Category */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'slug',
            'status',
        ],
    ]) ?>

</div>


<?php Modal::begin([
        'title' => '<h4>If you wonna save images select another category and click save.
                Click "Delete all", if you wonna delete category with all images</h4>',
        'toggleButton' => [
            'label' => 'Delete',
            'id'=> 'delete-category',
            'tag' => 'button',
            'class' => 'btn btn-danger',
        ],
      
    ]);
?>
    <?php $form = ActiveForm::begin(['id'=>'form-to-move']); ?>
    <div class="form-group">
        <?= $form->field($model, 'title')->dropDownList(ArrayHelper::map(Category::find()->all(),'slug', 'title'),
['promt'=>'Категория страницы', 'id'=> 'image-category']
    ) ?>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
 
    <?= Html::a('Delete all', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete category and all images?',
                'method' => 'post',
                
            ], 'id'=> 'form-to-delete'
        ]) ?>
