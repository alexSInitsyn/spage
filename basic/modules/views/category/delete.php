<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\models\category\Category;

?>
    <?php $form = ActiveForm::begin(); ?>

   
    <?= $form->field($model, 'category')->dropDownList(ArrayHelper::map(Category::find()->all(),'slug', 'title'),
['promt'=>'Категория страницы', 'id'=> 'image-category']
    ) ?>
    <?= $form->field($model, 'status')->dropDownList([1=>'guest', 2=>'user', 3=>'admin', 4=>'link'],
['promt'=>'Статус', 'id'=>'image-status']
    ) ?>

        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    

</div>
