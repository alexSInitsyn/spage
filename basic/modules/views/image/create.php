<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\models\image\Image */

$this->title = 'Create Image';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['/photo/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'uModel'=>$uModel
    ]) ?>

</div>
