<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\models\category\Category;

/* @var $this yii\web\View */
/* @var $model app\modules\models\image\Image */

$this->title = 'Update Image: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Images', 'url' => ['/photo/admin/images/'.$model->category]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="image-update">


    <?php $form = ActiveForm::begin(['id'=>'w0']); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true],['id' => 'image-title']) ?>

   <?= $form->field($model, 'author')->textInput(['maxlength' => true, 'id' => 'image-author']) ?>

    <?= $form->field($model, 'category')->dropDownList(ArrayHelper::map(Category::find()->all(),'slug', 'title'),
['promt'=>'Категория страницы', 'id'=> 'image-category']
    ) ?>
    <?= $form->field($model, 'status')->dropDownList([1=>'guest', 2=>'user', 3=>'admin', 4=>'link'],
['promt'=>'Статус', 'id'=>'image-status']
    ) ?>

        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    

</div>
