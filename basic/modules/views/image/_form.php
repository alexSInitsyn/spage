<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\models\category\Category;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\modules\models\image\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-form">

    <?php $form = ActiveForm::begin(['id'=>'image']); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true],['id' => 'image-title']) ?>

   <?= $form->field($model, 'author')->textInput(['maxlength' => true, 'id' => 'image-author']) ?>

    <?= $form->field($model, 'category')->dropDownList(ArrayHelper::map(Category::find()->all(),'slug', 'title'),
['promt'=>'Категория страницы', 'id'=> 'image-category']
    ) ?>
    <?= $form->field($model, 'status')->dropDownList([1=>'guest', 2=>'user', 3=>'admin', 4=>'link'],
['promt'=>'Статус', 'id'=>'image-status']
    ) ?>

<?= $form->field($model, 'image')->dropDownList([1=>'none', 2=>'left top', 3=>'right top', 4=>'left bottom', 5=>'rightbottom'],
['promt'=>'Статус', 'id'=>'image-watermark']
    ) ?>

    <?= $form->field($uModel, 'imageFile')->fileInput(['id'=>'image-file']) ?>
    <div class="form-group">

    

    
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    

</div>
