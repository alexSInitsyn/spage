<?php

namespace app\modules\models\category;

use Yii;
use app\modules\models\category\Category;
use app\modules\models\image\Image;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $status
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'status'], 'required'],
            [['title', 'slug', 'status'], 'string', 'max' => 255],
            ['slug', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/'],
            ['title', 'unique', 'targetClass' => Category::className()],
            ['slug', 'unique', 'targetClass' => Category::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'status' => 'Status',
        ];
    }

    


    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>['pageSize'=>10],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'title' => $this->title,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
           
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }


    public function getLogoUrl()
    {
        $logoId = Image::find()->andwhere(['category'=>$this->slug])->max('id');
        $logoExt = Image::find()->andwhere(['id'=>$logoId])->select('extension')->one();
       if($logoId ){
    
    return '/images/photogallery/'.$logoId.'.'.$logoExt->extension;
       }else{
           return false;
       }
    }


}
