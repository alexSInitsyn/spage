<?php

namespace app\modules\models\image;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property string $title
 * @property string $author
 * @property int $category
 * @property string $date
 * @property string $status
 * @property string $extension
 * @property string $image
 */
class Image extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'author', 'category', 'status', 'extension', 'image'], 'required'],
            ['title', 'unique', 'targetClass' => Image::className()],
           ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'author' => 'Author',
            'category' => 'Category',
            'date' => 'Date',
            'status' => 'Status',
            'extension'=> 'Extension',
            'image' => 'Watermark',
        ];
    }


  
 
    public function search($params)
    {
        $query = Image::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>['pageSize'=>10],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
           
            ->andFilterWhere(['like', 'author', $this->author])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'extension', $this->extension]);

        return $dataProvider;
    }

    public function getLogo($logoCat)
    {
    return Image::find()->andwhere(['category'=>$logoCat])->max('id');
    }

    public function getLogoUrl()
{
    $logoId = Image::find()->andwhere(['category'=>$this->category])->max('id');
    $logo = Image::find()->andwhere(['category'=>$this->category]);
   

return Yii::getAlias('@web').'images/photogallery/'.$logoId.'.'.$this->extension;
}

}
