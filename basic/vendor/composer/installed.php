<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => '8e5dda47f448c16a969aed50292bab1eaa4a804b',
    'name' => 'yiisoft/yii2-app-basic',
  ),
  'versions' => 
  array (
    'behat/gherkin' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.x-dev',
      ),
      'reference' => 'fb40504788cbc1b2ffde1d87703a9dab390559c6',
    ),
    'bower-asset/baguettebox.js' => 
    array (
      'pretty_version' => 'v1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '124bb63cf1a11504b60b570b49d8e0c7171ed4e9',
    ),
    'bower-asset/bootstrap' => 
    array (
      'pretty_version' => 'v3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '68b0d231a13201eb14acd3dc84e51543d16e5f7e',
    ),
    'bower-asset/inputmask' => 
    array (
      'pretty_version' => '3.3.11',
      'version' => '3.3.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e670ad62f50c738388d4dcec78d2888505ad77b',
    ),
    'bower-asset/jquery' => 
    array (
      'pretty_version' => '3.6.0',
      'version' => '3.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e786e3d9707ffd9b0dd330ca135b66344dcef85a',
    ),
    'bower-asset/punycode' => 
    array (
      'pretty_version' => 'v1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '38c8d3131a82567bfef18da09f7f4db68c84f8a3',
    ),
    'bower-asset/yii2-pjax' => 
    array (
      'pretty_version' => '2.0.7.1',
      'version' => '2.0.7.1',
      'aliases' => 
      array (
      ),
      'reference' => 'aef7b953107264f00234902a3880eb50dafc48be',
    ),
    'cebe/markdown' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.2.x-dev',
      ),
      'reference' => '2b2461bed9e15305486319ee552bafca75d1cdaa',
    ),
    'codeception/codeception' => 
    array (
      'pretty_version' => '4.1.x-dev',
      'version' => '4.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2585340b4cb9f194f8cf1202495d2e76ef6e803c',
    ),
    'codeception/lib-asserts' => 
    array (
      'pretty_version' => '1.13.2',
      'version' => '1.13.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '184231d5eab66bc69afd6b9429344d80c67a33b6',
    ),
    'codeception/lib-innerbrowser' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '31b4b56ad53c3464fcb2c0a14d55a51a201bd3c2',
    ),
    'codeception/module-asserts' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '59374f2fef0cabb9e8ddb53277e85cdca74328de',
    ),
    'codeception/module-filesystem' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'cc34791c9d56d6481fb033e7999d728b71415a11',
    ),
    'codeception/module-yii2' => 
    array (
      'pretty_version' => '1.1.5',
      'version' => '1.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '14269d059b8eaedf3d414a673907bd874cd4ed04',
    ),
    'codeception/phpunit-wrapper' => 
    array (
      'pretty_version' => '8.1.4',
      'version' => '8.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f41335f0b4dd17cf7bbc63e87943b3ae72a8bbc3',
    ),
    'codeception/specify' => 
    array (
      'pretty_version' => '0.4.6',
      'version' => '0.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '21b586f503ca444aa519dd9cafb32f113a05f286',
    ),
    'codeception/stub' => 
    array (
      'pretty_version' => '3.7.0',
      'version' => '3.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '468dd5fe659f131fc997f5196aad87512f9b1304',
    ),
    'codeception/verify' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0bb946b6d61279f461bcc5a677ac0ed5eab9b3',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.5.x-dev',
      'version' => '1.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '10dcfce151b967d20fde1b34ae6640712c3891bc',
    ),
    'eluhr/yii2-baguette-box' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '055950b595b2f99eba06aedd97c85bee76e0b2da',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.14.0',
      'version' => '4.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ab42bd6e742c70c0a52f7b82477fcd44e64b75',
    ),
    'fakerphp/faker' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
        0 => '1.20.x-dev',
      ),
      'reference' => '987b51effeeb347fd7bbbf660c5151d870a4853c',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.2.x-dev',
      ),
      'reference' => '5c693242bede743c23402bc5b9de62da04a882d7',
    ),
    'imagine/imagine' => 
    array (
      'pretty_version' => 'dev-develop',
      'version' => 'dev-develop',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => '9b9aacbffadce8f19abeb992b8d8d3a90cc2a52a',
    ),
    'kop/yii2-scroll-pager' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '27720921e60b5df12b73c31429472fe8781e0425',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '14daed4296fae74d9e3201d2c4925d1acb7aa614',
    ),
    'npm-asset/bootstrap' => 
    array (
      'pretty_version' => '4.6.1',
      'version' => '4.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '23ba782b30a44914f6409ed78ff37292e9a83561',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '36d8a21e851a9512db2b086dc5ac2c61308f0138',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.x-dev',
      ),
      'reference' => 'a0eeab580cbdf4414fef6978732510a36ed0a9d6',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '5.x-dev',
      ),
      'reference' => '9455bde915e322a823d464a2c41e5c0de03512a6',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.x-dev',
      'version' => '1.9999999.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '77a32518733312af16a44300404e945338981de3',
    ),
    'phpspec/php-diff' => 
    array (
      'pretty_version' => 'v1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc1156187f9f6c8395886fe85ed88a0a245d72e9',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.x-dev',
      ),
      'reference' => 'bbcd7380b0ebf3961ee21409db7b38bc31d69a13',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.x-dev',
      'version' => '7.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '819f92bba8b001d4363065928088de22f25a3a48',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '42c5ba5220e6904cbfe8b1a1bda7c0cfdc8c12f5',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.x-dev',
      'version' => '2.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '4.0.x-dev',
      ),
      'reference' => '76fc0567751d177847112bd3e26e4890529c98da',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.x-dev',
      'version' => '8.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '690d0c79d1584c3cab3b97ca135eec9feb386beb',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2ae37329ee82f91efadc282cc2d527fd6065a5ef',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'aa4f89e91c423b516ff226c50dc83f824011c253',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => '36fa03d50ff82abcae81860bdaf4ed9a1510c7cd',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.0.x-dev',
      ),
      'reference' => 'efd67d1dc14a7ef4fc4e518e7dee91c271d524e4',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.x-dev',
      'version' => '1.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '1071dfcef776a57013124ff35e1fc41ccd294758',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.x-dev',
      'version' => '4.2.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'a8cb2aa3eca438e75a4b7895f04bc8f5f990bc49',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.x-dev',
      'version' => '3.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '77200c5b1b7b073a04aca7f593a26dad7df1de35',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'de036ec91d55d2a9e0db2ba975b512cdb1c23921',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.x-dev',
      'version' => '3.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.x-dev',
      'version' => '2.0.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.x-dev',
      'version' => '1.1.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '49529de1ea06aa1f7d30e9c6ce2d196575ff56ad',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'symfony/browser-kit' => 
    array (
      'pretty_version' => 'v4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '61d85c5af2fc058014c7c89504c3944e73a086f0',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '900275254f0a1a2afff1ab0e11abd5587a10e1d6',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'b0a190285cd95cb019237851205b8140ef6e368e',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => '2.5.x-dev',
      'version' => '2.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
    ),
    'symfony/dom-crawler' => 
    array (
      'pretty_version' => '4.4.x-dev',
      'version' => '4.4.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => '4e9215a8b533802ba84a3cc5bd3c43103e7a6dc3',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'dec8a9f58d20df252b9cd89f1c6c1530f747685d',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => '2.5.x-dev',
      'version' => '2.5.9999999.9999999-dev',
      'aliases' => 
      array (
      ),
      'reference' => 'f98b54df6ad059855739db6fcbc2d36995283fe1',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '231313534dded84c7ecaa79d14bc5da4ccb69b7d',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '81b86b50cf841a64252b439e738e97f4a34e2783',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc5db0e22b3cb4111010e48785a97f670b350ca5',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '191afdcb5804db960d26d8566b7e9a2843cab3a0',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => '92043b7d8383e48104e411bc9434b260dbeb5a10',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => '5.4.x-dev',
      'version' => '5.4.9999999.9999999-dev',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'e80f87d2c9495966768310fc531b487ce64237a2',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
    ),
    'webcreate/jquery-ias' => 
    array (
      'pretty_version' => 'v2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1deffb3b56a170098a53a06aeff66674146c09b6',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '1.10.x-dev',
      ),
      'reference' => 'b419d648592b0b8911cbbe10d450fe314f4fd262',
    ),
    'yiisoft/yii2' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'a17ec7376309f707b3a636e607fdbfd69ef521d8',
    ),
    'yiisoft/yii2-app-basic' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => '8e5dda47f448c16a969aed50292bab1eaa4a804b',
    ),
    'yiisoft/yii2-bootstrap' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => 'bdbaa9de468f480a49f12849a101fd91b755f050',
    ),
    'yiisoft/yii2-bootstrap4' => 
    array (
      'pretty_version' => '2.0.10',
      'version' => '2.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e6d0e58f43d3910129d554ac183aac17f65be639',
    ),
    'yiisoft/yii2-composer' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '0933fd949a036348283322024948541324cce66a',
    ),
    'yiisoft/yii2-debug' => 
    array (
      'pretty_version' => '2.1.19',
      'version' => '2.1.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '84d20d738b0698298f851fcb6fc25e748d759223',
    ),
    'yiisoft/yii2-faker' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '2.0.x-dev',
      ),
      'reference' => '98e4e4ca1aa38282af598dd2e7f658c7090d22e5',
    ),
    'yiisoft/yii2-gii' => 
    array (
      'pretty_version' => '2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd879cb186361fbc6f71a2d994d580b5a071a5642',
    ),
    'yiisoft/yii2-imagine' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8637f63ee2eb81b7fe1f4c0e16cc8cc474a68bae',
    ),
  ),
);
