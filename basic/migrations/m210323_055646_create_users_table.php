<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m210323_055646_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'author' => $this->string()->notNull(),
            'category' => $this->string()->notNull(),
            'date' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
            'status' => $this->string()->notNull(),
            'extension' => $this->text()->notNull(),
            ]);

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'status' => $this->string()->notNull(),
            'count'=> $this->integer()->notNull(),
             ]);

                   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%image}}');
        $this->dropTable('{{%category}}');
        }
}
